#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int generate(){
    int size = 10000;
    int inside = 0;
    int x, y = 0;
    long total = 100000;
    int dist = 0;
    float pi = 0;

    for(int i = 0; i < total; i++){
        x = rand() % size;
        y = rand() % size;


        dist = x * x + y * y;

        inside += (dist < size * size);
    }

    pi = 4 * (inside / total);
    
    printf("Pi: %f\n", pi);
}

int main(){
    srand(time(NULL));
    generate();
    return 0;
}
